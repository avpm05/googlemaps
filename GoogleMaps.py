from googleplaces import GooglePlaces, lang

import json
KeyAPI = "AIzaSyBmqXSKO98HOOw_c-I0Z9wTdnuZN2WBZmo"
results = {}
google_places = GooglePlaces(KeyAPI)

def get_places(address, term, radius):
    query_result = google_places.nearby_search(location=address,language=lang.RUSSIAN, keyword=term,radius=radius)
    return len(query_result.places)

Pokazateli  = {
"vuzs":"Высшее учебное заведение",
"schools":"Общеобразовательная Школа",
"detsad":"Детский сад",
"bibl": "Библиотека",
"magazine":"Магазин",
"bituslugi":"Бытовые услуги",
"park":"Парк",
"fitnes":"Спортинвый комплекс",
"sport": "Спортивная площадка",
"detploshad":"Детская площадка",
"policlinika":"Поликлиника",
"tc":"Торговый центр",
"apteka":"Аптека",
"obcepit":"Пункт Общественного питания",
"bank":"Банк",
"bankomat":"Круглосуточный банкомат",
"bassein":"Водный объект",
"teatr":"Театр",
"religobject":"Религиозный объект"
}

address = input("Write address:")

for name in Pokazateli:
    results[name]= get_places(address,Pokazateli[name],1000)

for r in results:
    print(r + "----" + str(results[r]))

